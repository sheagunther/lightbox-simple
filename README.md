# README #


### What is this repository for? ###

* This runs a home built lightbox using Neopxel LEDs. The physical controls consist of one on/off toggle switch and two potentiometers
* used to control the mode and to set different settings like speed and color choice. 
* 

### How do I get set up? ###

* Written in Arduino IDE. 



### Who do I talk to? ###

* Shea Gunther can be contacted at SheaGunther@gmail.com.